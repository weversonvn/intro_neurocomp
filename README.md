## Introdução à neurociência computacional com a linguagem Python

### Descrição

Este repositório contém os arquivos de um roteiro a ser usado para um curso introdutório de neurociência computacional. O cérebro é um órgão complexo, despertando grande interesse na compreensão dos mecanismos biológicos subjacentes ao seu funcionamento. A neurociência computacional é um campo de estudo que busca contribuir para essa compreensão. O curso introdutório é destinado a alunos de graduação interessados em adquirir conhecimentos básicos em Neurociência Computacional. Inicialmente, o curso fornece uma base teórica, abrangendo tanto aspectos neurofisiológicos quanto matemáticos e algorítmicos, permitindo que estudantes de diversas áreas aproveitem o conteúdo com o mínimo de pré-requisitos. Em seguida, o curso apresenta modelos de neurônios, desde os mais simples até os mais elaborados, explorando como esses neurônios se conectam, incluindo circuitos conhecidos de conexões neuronais e a implementação do aprendizado nessas redes. Além disso, o curso aborda temas de inteligência artificial, como redes neurais e neuromórficas, estas últimas utilizando os modelos mencionados anteriormente. Utiliza códigos interativos em linguagem Python, de natureza livre e código aberto, para as simulações do conteúdo apresentado.

### Como usar

Os arquivos base estão no formato de _notebooks_ do _Jupyter_, na linguagem _Python_. O uso dos _notebooks_ pode ser feito através do [Google Colaboratory (Colab)](https://colab.research.google.com) ou de uma instalação local do _Python_ com o _Jupyter Notebook_ e os pacotes descritos nos respectivos _notebooks_. 

Para uso com o Colab (recomendado):

1. Baixe o repositório clicando no botão azul `code`, e depois `Download source code - zip`
1. Descompacte o arquivo baixado
1. Abra o Colab e selecione a opção `Upload`, e selecione o _notebook_ desejado

Se desejar, é possível baixar cada _notebook_ individualmente. O sumário é como segue:

1. [Tutorial Python](src/python.ipynb)
1. [Referencial teórico](src/ref_teorico.ipynb)
1. [Modelos de neurônio de disparo](src/modelos_disparo.ipynb)
1. [Conexões entre neurônios](src/conexoes.ipynb)
1. [Inteligência artificial](src/inteligencia.ipynb)

> Para instalações locais, a instalação dos pacotes pode ser feita através do comando `pip install -r requirements.txt`, que usa o arquivo [`requirements.txt`](requirements.txt) para instalar os pacotes necessários.

### Licença

Copyright 2021 Weverson Nascimento

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
