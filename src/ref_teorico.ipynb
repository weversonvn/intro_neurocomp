{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6843b2f9",
   "metadata": {},
   "source": [
    "# Referencial teórico"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "5d1d2d89",
   "metadata": {},
   "outputs": [],
   "source": [
    "# os pacotes abaixo são necessários para a execução dos exemplos\n",
    "# linhas iniciadas por # são comentários\n",
    "# e são ignoradas pelo interpretador Python\n",
    "\n",
    "import numpy as np # processamento matemático\n",
    "import matplotlib.pyplot as plt # criação de gráficos\n",
    "from scipy.integrate import solve_ivp # solucionar eq. dif."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6305e82c",
   "metadata": {},
   "source": [
    "## Neurobiologia básica\n",
    "\n",
    "O neurônio, mostrado na figura abaixo, é a unidade básica do sistema nervoso. É composto pelo corpo celular (chamado de soma), dendritos, que recebem sinais de outros neurônios, axônio, que transmite os sinais a outros neurônios, e terminais axônicos, que são as extremidades do axônio e se conectam com os dendritos de outros neurônios. Como outras células do corpo humano, o neurônio é composto por íons e moléculas, ambos podendo possuir cargas positivas ou negativas. A célula neuronal é revestida por uma bi-camada lipídica, e geralmente o interior dela possui uma maior concentração de cargas negativas, fazendo com que o potencial de membrana ($V_m$), que é a diferença de potencial entre a parte interna e a externa da célula neuronal ($V_M=V_{dentro}-V_{fora}$), fique a maior parte do tempo com valor negativo. O potencial de membrana se altera quando há o fluxo de íons através dos poros de canais iônicos.\n",
    "\n",
    "![Neurônio](https://gitlab.com/weversonvn/intro_neurocomp/-/raw/main/src/imagens/neuronio.png)\n",
    "\n",
    "Os canais iônicos são canais proteicos na membrana da célula neuronal e permitem a movimentação de íons através deles, como mostrado abaixo. Podem ser de dois tipos: com ou sem portão. Canais sem portão estão sempre abertos, enquanto os com portão podem abrir ou fechar, dependendo do valor do potencial de membrana, e por isso são chamados de canais iônicos dependentes de tensão. Quando o fluxo de corrente elétrica de todos os íons é equilibrado dentro e fora da célula neuronal (ou seja, o potencial de membrana não se altera), ele é chamado de potencial de repouso (ou de equilíbrio). O valor típico é próximo de $-70\\ mV$. O valor do potencial de repouso ($E_A$) é calculado com base na concentração de cada íon dentro e fora da célula neuronal usando a equação de Nernst, dada por:\n",
    "$$\n",
    "E_A=\\frac{k_BT}{z_Aq_e}ln\\left(\\frac{A_{fora}}{A_{dentro}}\\right)\n",
    "$$\n",
    "sendo $A$ o íon, $z_A$ a carga do íon, $A_{fora}$ e $A_{dentro}$ a concentração desse íon fora e dentro da célula neuronal, respectivamente, $T$ a temperatura absoluta (em Kelvin), $k_B$ a a constante de \\textit{Boltzmann} ($1,39\\text{x}10^{-23}\\ JK^{-1}$) e $q_e$ a carga elétrica fundamental ($1,6\\text{x}10^{-19}\\ C$). Os valores de cargas, concentração e potencial de Nernst para alguns íons são apresentados na Tabela~\\ref{tab:concentracao_nernst}.\n",
    "\n",
    "![Membrana do neurônio](https://gitlab.com/weversonvn/intro_neurocomp/-/raw/main/src/imagens/membrana_neuronio.png)\n",
    "\n",
    "Ion | Carga | Concentração interna (nM) | Concentração externa (nM) | Potencial de reversão (mV)\n",
    ":---: | :---: | :---: | :---: | :---:\n",
    "Sódio | +1 | 15 | 120 | 61,6\n",
    "Potássio | +1 | 150 | 6 | -86,1\n",
    "Cloreto | -1 | 52 | 560 | -65\n",
    "Cálcio | +2 | 50 | 2 | 141,7\n",
    "\n",
    "\n",
    "Fora do repouso, a diferença de potencial entre o interior e o exterior do neurônio produz movimentação iônica. Quando íons positivos (como $Na^+$ ou $Ca^{2+})$ entram na célula neuronal, o potencial de membrana fica menos negativo (até próximo de 0 mV), fenômeno conhecido como despolarização. De maneira semelhante, quando íons positivos (como $K^+$) saem da célula neuronal, ou negativos (como $Cl^-$) entram, o potencial de membrana fica mais negativo, fenômeno conhecido como hiperpolarização. Os canais de potássio são mais presentes na membrana do neurônio, sendo os principais responsáveis pelo potencial de equilíbrio. Já os canais de sódio, associados à despolarização, são responsáveis pela geração do potencial de ação. Devido ao excesso de cargas negativas no interior da célula neuronal, e a existência de cargas positivas no exterior, a membrana do neurônio acaba funcionando como um capacitor, criando uma capacitância ($C_m$). O fluxo de íons através de canais sem portão é considerado constante, podendo ser agrupado em um elemento chamado de vazamento (\\textit{leak}, em inglês), possuindo um potencial ($E_l$) e uma condutância ($G_l$), que é a facilidade desse elemento permitir o fluxo de corrente (o inverso da resistência). Com isso, é possível escrever uma equação relacionando o potencial de membrana da célula neuronal e os elementos de vazamento, como segue:\n",
    "$$\n",
    "\t\\frac{\\mathrm{d}V_m}{\\mathrm{d}t}=G_l(E_l-V_m)/C_m\n",
    "$$\n",
    "que é dada na forma de uma equação diferencial, detalhada na seção seguinte."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2bef014c",
   "metadata": {},
   "source": [
    "## Equações diferenciais ordinárias\n",
    "\n",
    "As equações diferenciais são usadas para descrever sistemas dinâmicos, ou seja, que se alteram ao longo do tempo. A forma mais básica de uma equação diferencial é:\n",
    "$$\n",
    "\t\\frac{\\mathrm{d}y(t)}{\\mathrm{d}t}=F(t,y(t))\n",
    "$$\n",
    "com o lado esquerdo da equação sendo a derivada de $y(t)$, a função presente no lado direito associada à variável $t$, que geralmente representa o tempo, e $F$ representando uma função qualquer com $t$ e $y$. Diferentemente das equações algébricas, que possuem como solução um número, as equações diferenciais tem uma família de funções como solução, variando de acordo com a escolha de constantes, como mostrado na figura abaixo. Nela, é exibida a família de soluções $y(t) = t + 1 + ce^t$ para a equação diferencial $y'=y-t$. $y'$ é uma representação equivalente para $\\frac{\\mathrm{d}y(t)}{\\mathrm{d}t}$. Qualquer valor de $c$ real que for escolhido representa uma solução possível para a equação, sendo exibidas as curvas para os valores de $c$ iguais a $0$, $-1$ e $0,4$.\n",
    "\n",
    "![Soluções da equação diferencial](https://gitlab.com/weversonvn/intro_neurocomp/-/raw/main/src/imagens/solucao.png)\n",
    "\n",
    "### Exemplos\n",
    "\n",
    "#### Decaimento radioativo\n",
    "Segundo a lei do decaimento radioativo, a taxa na qual os átomos radioativos se desintegram é proporcional ao número total de átomos radioativos presente. Sendo $N(t)$ o número de átomos radioativos no tempo $t$, então $N'(t)$ é a taxa de mudança. A lei do decaimento radioativo é a que segue:\n",
    "$$\n",
    "\tN'(t) = -\\lambda N(t)\n",
    "$$\n",
    "onde $\\lambda$ é a constante de decaimento. O gráfico de uma possível solução para essa equação diferencial é gerado pelo código abaixo."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1797dcbc",
   "metadata": {},
   "outputs": [],
   "source": [
    "def decaimento(t, N):\n",
    "    lamb = 0.5\n",
    "    return -lamb * N\n",
    "\n",
    "t0 = 0 # tempo inicial\n",
    "n0 = 20 # valor inicial\n",
    "tf = 10 # tempo final\n",
    "dt = 0.001 # passo de integração\n",
    "tvec = np.arange(t0, tf, dt) # vetor de tempo\n",
    "\n",
    "# resolve a equação diferencial\n",
    "sol = solve_ivp(decaimento, [t0, tf], [n0], t_eval=tvec)\n",
    "\n",
    "# gera e exibe o gráfico\n",
    "plt.plot(sol.t, sol.y[0])\n",
    "plt.xlabel('Tempo')\n",
    "plt.ylabel('N(t)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "836558fb",
   "metadata": {},
   "source": [
    "#### Equações de Lotka-Volterra\n",
    "\n",
    "Também conhecidas como equações predador-presa, são um par de equações diferenciais de primeira ordem, frequentemente usadas para descrever a dinâmica de sistemas biológicos de interação entre duas espécies, uma como predadora e a outra como presa. As populações de cada uma das espécies são dadas pelo par de equações:\n",
    "$$\n",
    "\tx' = ax - bxy\n",
    "$$\n",
    "\n",
    "$$\n",
    "\ty' = dxy - cy\n",
    "$$\n",
    "sendo $x$ a população da presa, $y$ a do predador, $x',\\ y'$ as taxas de variação de cada população, e $a,\\ b,\\ c,\\ d$ os parâmetros que descrevem a interação entre as espécies. Abaixo temos o código que gera a solução do par de equações para um valor de cada parâmetro de interação."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "00772871",
   "metadata": {},
   "outputs": [],
   "source": [
    "def lotkavolterra(t, z, a, b, c, d):\n",
    "    x, y = z\n",
    "    return a*x - b*x*y, -c*y + d*x*y\n",
    "\n",
    "# resolve as equações passando os parâmetros a, b, c, d\n",
    "sol = solve_ivp(lotkavolterra, [0, 15], [10, 5], args=(1.5, 1, 3, 1),\n",
    "                dense_output=True)\n",
    "t = np.linspace(0, 15, 300) # vetor de tempo\n",
    "z = sol.sol(t) # solução para os pontos do vetor de tempo\n",
    "\n",
    "# gera e exibe o gráfico\n",
    "plt.plot(t, z.T)\n",
    "plt.xlabel('t')\n",
    "plt.ylabel('população')\n",
    "# coloca legenda para o par de equações\n",
    "plt.legend(['presa', 'predador'], shadow=True)\n",
    "plt.title('Sistema de Lotka-Volterra')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "587b259b",
   "metadata": {},
   "source": [
    "### Método de Euler\n",
    "\n",
    "Equações diferenciais ordinárias podem ser resolvidas analiticamente, não abordado neste texto, ou numericamente. Dentre os vários métodos existentes para a solução numérica, a adotada aqui é o método de Euler. Considerando a equação $\\frac{dx}{dt}=f(x,t)$, com $f(x,t)$ uma função qualquer de $x$ em relação a $t$, dado um valor inicial $x0$ (usualmente com $t=0$), é possível simular a equação usando pontos discretos com intervalos $\\Delta t$ fixos. Cada valor $x_n$ é dado por $x_n=x(t_n=n\\Delta t)$. A partir disso, é possível usar o método de Euler avançado para calcular um valor seguinte a partir do valor anterior, ou seja:\n",
    "$$\n",
    "x_{n+1}=x_n+f(x_n,t_n)\\Delta t\n",
    "$$\n",
    "como é demonstrado na figura abaixo. Outros métodos não abordados no curso incluem o método de Euler reverso e o Runge-Kutta de segunda e quarta ordens, que são mais precisos na solução.\n",
    "\n",
    "![Método de Euler](https://gitlab.com/weversonvn/intro_neurocomp/-/raw/main/src/imagens/euler.png)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "neuro",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
